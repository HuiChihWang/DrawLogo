//
//  ContentView.swift
//  DrawLogo
//
//  Created by Hui Chih Wang on 2021/3/22.
//

import SwiftUI

struct DrawView: UIViewRepresentable {
    let aDegree = CGFloat.pi / 180
    
    func makeUIView(context: Context) -> UIView {
        let view = UIView()
        
        let centerTopLeaf = UIBezierPath()
        centerTopLeaf.move(to: CGPoint(x: 260, y: 26))
        centerTopLeaf.addQuadCurve(to: CGPoint(x: 230, y: 135), controlPoint: CGPoint(x: 225, y: 80))
        centerTopLeaf.addLine(to: CGPoint(x: 290, y: 135))
        centerTopLeaf.addQuadCurve(to: CGPoint(x: 260, y: 26), controlPoint: CGPoint(x: 295, y: 80))
        shiftPathToCenter(path: centerTopLeaf)
        let layerCenterTopLeaf = CAShapeLayer()
        layerCenterTopLeaf.path = centerTopLeaf.cgPath
        layerCenterTopLeaf.fillColor = #colorLiteral(red: 0.8907176852, green: 0.001977676526, blue: 0.06022591889, alpha: 1).cgColor
        view.layer.addSublayer(layerCenterTopLeaf)
        
        
        let yellowBar = UIBezierPath()
        yellowBar.move(to: CGPoint(x: 150, y: 145))
        yellowBar.addLine(to: CGPoint(x: 370, y: 145))
        yellowBar.addQuadCurve(to: CGPoint(x: 365, y: 155), controlPoint: CGPoint(x: 372, y: 150))
        yellowBar.addLine(to: CGPoint(x: 155, y: 155))
        yellowBar.addQuadCurve(to: CGPoint(x: 150, y: 145), controlPoint: CGPoint(x: 148, y: 150))
        shiftPathToCenter(path: yellowBar)
        let layerYellowBar = CAShapeLayer()
        layerYellowBar.path = yellowBar.cgPath
        layerYellowBar.fillColor = #colorLiteral(red: 0.9991862178, green: 0.8652842641, blue: 0.01491379458, alpha: 1).cgColor
        view.layer.addSublayer(layerYellowBar)


        let purpleBar = UIBezierPath()
        purpleBar.move(to: CGPoint(x: 165, y: 165))
        purpleBar.addLine(to: CGPoint(x: 355, y: 165))
        purpleBar.addQuadCurve(to: CGPoint(x: 345, y: 175), controlPoint: CGPoint(x: 357, y: 170))
        purpleBar.addLine(to: CGPoint(x: 175, y: 175))
        purpleBar.addQuadCurve(to: CGPoint(x: 165, y: 165), controlPoint: CGPoint(x: 163, y: 170))
        shiftPathToCenter(path: purpleBar)
        let layerPurpleBar = CAShapeLayer()
        layerPurpleBar.path = purpleBar.cgPath
        layerPurpleBar.fillColor = #colorLiteral(red: 0.6008570194, green: 0.2880139053, blue: 0.5917795897, alpha: 1).cgColor
        view.layer.addSublayer(layerPurpleBar)
        
        let centerBottomLeaf = UIBezierPath()
        let centerX: CGFloat = 260
        let radius: CGFloat = 20
        centerBottomLeaf.move(to: CGPoint(x: centerX - radius, y: 185))
        centerBottomLeaf.addLine(to: CGPoint(x: centerX + radius, y: 185))
        centerBottomLeaf.addQuadCurve(to: CGPoint(x: centerX, y: 205), controlPoint: CGPoint(x: centerX + 15, y: 195))
        centerBottomLeaf.addQuadCurve(to: CGPoint(x: centerX - radius, y: 185), controlPoint: CGPoint(x: centerX - 15, y: 195))
        shiftPathToCenter(path: centerBottomLeaf)
        let layerCenterBottomLeaf = CAShapeLayer()
        layerCenterBottomLeaf.path = centerBottomLeaf.cgPath
        layerCenterBottomLeaf.fillColor = #colorLiteral(red: 0.0876011923, green: 0.6959455609, blue: 0.7166442871, alpha: 1).cgColor
        view.layer.addSublayer(layerCenterBottomLeaf)
        
        let leftTopLeaf = UIBezierPath()
        var start = CGPoint(x: 135, y: 75)
        leftTopLeaf.move(to: start)
        leftTopLeaf.addQuadCurve(to: CGPoint(x: 145, y: 135), controlPoint: CGPoint(x: 135, y: 100))
        leftTopLeaf.addLine(to: CGPoint(x: 220, y: 135))
        leftTopLeaf.addQuadCurve(to: start, controlPoint: CGPoint(x: 190, y: 90))
        let rightTopLeaf = mirrorXPath(path: leftTopLeaf, with: CGPoint(x: 260, y: 0))

        shiftPathToCenter(path: leftTopLeaf)
        let layerLeftTopLeaf = CAShapeLayer()
        layerLeftTopLeaf.path = leftTopLeaf.cgPath
        layerLeftTopLeaf.fillColor = #colorLiteral(red: 0.9532384276, green: 0.5543913841, blue: 0, alpha: 1).cgColor
        view.layer.addSublayer(layerLeftTopLeaf)

        shiftPathToCenter(path: rightTopLeaf)
        let layerRightTopLeaf = CAShapeLayer()
        layerRightTopLeaf.path = rightTopLeaf.cgPath
        layerRightTopLeaf.fillColor = #colorLiteral(red: 0.08897291869, green: 0.7038971782, blue: 0.7245384455, alpha: 1).cgColor
        view.layer.addSublayer(layerRightTopLeaf)

        let leftＢottomLeaf = UIBezierPath()
        start = CGPoint(x: 190, y: 185)
        leftＢottomLeaf.move(to: start)
        leftＢottomLeaf.addQuadCurve(to: CGPoint(x: 240, y: 205), controlPoint: CGPoint(x: 220, y: 200))
        leftＢottomLeaf.addQuadCurve(to: CGPoint(x: 235, y: 185), controlPoint: CGPoint(x: 240, y: 200))
        leftＢottomLeaf.addLine(to: start)
        let rightＢottomLeaf = mirrorXPath(path: leftＢottomLeaf, with: CGPoint(x: 260, y: 0))

        shiftPathToCenter(path: leftＢottomLeaf)
        let layerLeftBottomLeaf = CAShapeLayer()
        layerLeftBottomLeaf.path = leftＢottomLeaf.cgPath
        layerLeftBottomLeaf.fillColor = #colorLiteral(red: 0.9571456313, green: 0.6854690313, blue: 0.7619815469, alpha: 1).cgColor
        view.layer.addSublayer(layerLeftBottomLeaf)

        shiftPathToCenter(path: rightＢottomLeaf)
        let layerRightBottomLeaf = CAShapeLayer()
        layerRightBottomLeaf.path = rightＢottomLeaf.cgPath
        layerRightBottomLeaf.fillColor = #colorLiteral(red: 0.8864466548, green: 0.005615013652, blue: 0.06151982397, alpha: 1).cgColor
        view.layer.addSublayer(layerRightBottomLeaf)

        
        return view
    }
    
    func shiftPathToCenter(path: UIBezierPath) {
        let screenSize = UIScreen.main.bounds.size
        let centerX = screenSize.width / 2
        path.apply(CGAffineTransform(translationX: centerX - 260, y: 200))
    }
    
    func mirrorXPath(path: UIBezierPath, with center: CGPoint) -> UIBezierPath {
        let path = UIBezierPath(cgPath: path.cgPath)
        var mirrorTransform = CGAffineTransform.identity

        mirrorTransform = mirrorTransform.translatedBy(x: center.x, y: 0)
        mirrorTransform = mirrorTransform.scaledBy(x: -1, y: 1)
        mirrorTransform = mirrorTransform.translatedBy(x: -center.x, y: 0)
                
        path.apply(mirrorTransform)
        return path
    }
    
    func updateUIView(_ uiView: UIView, context: Context) {
        
    }
        
}
struct ContentView: View {
    private func createTextView() -> some View {
        HStack(spacing: 1) {
            Text("a")
                .foregroundColor(Color(#colorLiteral(red: 0.09818977863, green: 0.6997078657, blue: 0.7290913463, alpha: 1)))
            Text("d")
                .foregroundColor(Color(#colorLiteral(red: 0.8864404559, green: 0.005858433433, blue: 0.05218402296, alpha: 1)))
            Text("i")
                .foregroundColor(Color(#colorLiteral(red: 0.9478831887, green: 0.5632705092, blue: 0.004960162565, alpha: 1)))
            Text("d")
                .foregroundColor(Color(#colorLiteral(red: 0.9984415174, green: 0.8694395423, blue: 0.007266299333, alpha: 1)))
            Text("a")
                .foregroundColor(Color(#colorLiteral(red: 0.6048167348, green: 0.292259872, blue: 0.591563046, alpha: 1)))
            Text("s")
                .foregroundColor(Color(#colorLiteral(red: 0.956464231, green: 0.6899386644, blue: 0.7530820966, alpha: 1)))
        }
        .font(.custom("TeXGyreAdventor-Bold", size: 90))
    }
    
    var body: some View {
        ZStack {
            Color.black
            VStack {
                DrawView()
                createTextView()
                    .offset(y: -375)
            }
        }
        .ignoresSafeArea()
    }
}
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
